package com.tl.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project

class HiMan implements Plugin<Project> {

  @Override
  void apply(Project project) {
    project.task('himan-hello') {
      if ("${project.showConfigPhase}".toBoolean()) {
        println "Configure ${project.name} for ${name}"
      }
      doLast {
        println "Hello ${name} ${project.name}"
      }
    }
    project.task('himan-bye') {
      if ("${project.showConfigPhase}".toBoolean()) {
        println "Configure ${project.name} for ${name}"
      }
      doLast {
        println "Bye ${name} ${project.name}"
      }
    }
  }
}
